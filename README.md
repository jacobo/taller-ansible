## Automatización, configuración y administración de computadoras con Ansible

En nuestra próxima sesión (9 de julio, 12 hrs hora central de México) de Ansible con el Technology Infrastructure and Services Team vamos a conversar y explorar sobre los principales conceptos detras de Ansible y las posibilidades de esta tecnología, como algunas referencias de inspiración para profundizar en la automatización de infraestructuras y redes de computadoras.  

- Breve revisión sobre los tipos de automatización 
- Revisión sobre los conceptos básicos  de Ansible 
- Ejemplos de uso 
- Referencias

### Tipos de automatización 

- [Imperativa (cómo)](apache2.yml)
- [Declarativa (qué)](tarea.yml)

### Conceptos de Ansible 

- [Tareas](tarea.yml)
- [Libros de jugadas](jugada.yml) 
- [Inventarios](inventarios)
- [Roles](https://github.com/geerlingguy/ansible-role-mysql) 
- Colecciones 

### Casos de uso 

- [Debian Conference 2020](https://salsa.debian.org/debconf-video-team/ansible) 
- [Instalación y operación de proxies de la red Tor](https://www.jacobo.org/ansible-snowflake/)
- Primero de Mayo 

### Referencias 

- [Ansible for devops, repositorio](https://github.com/geerlingguy/ansible-for-devops)
- [Ansible for devops - libro](https://www.ansiblefordevops.com/)
- [Colecciones de Ansible](https://www.jacobo.org/colecciones-de-ansible/)
- [Ansible role de Caddy](https://galaxy.ansible.com/nvjacobo/caddy)
